%global _empty_manifest_terminate_build 0
Name:		python-moto
Version:	5.0.23
Release:	1
Summary:	A library that allows your python tests to easily mock out the boto library
License:	Apache-2.0
URL:		https://github.com/spulec/moto
Source0: 	https://files.pythonhosted.org/packages/07/a6/0a970920b4fcb95ff1914e2f58f1c54d3a813ec4746ffee97b4289a218e0/moto-5.0.23.tar.gz
BuildArch:	noarch

%description
Moto is a library that allows your tests to easily mock out AWS Services.

%package -n python3-moto
Summary:	A library that allows your python tests to easily mock out the boto library
Provides:	python-moto = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
BuildRequires:	python3-hatchling
%description -n python3-moto
Moto is a library that allows your tests to easily mock out AWS Services.

%package help
Summary:	Development documents and examples for moto
Provides:	python3-moto-doc
%description help
Moto is a library that allows your tests to easily mock out AWS Services.

%prep
%autosetup -n moto-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%files -n python3-moto
%license LICENSE
%doc README.md
%{_bindir}/moto_proxy
%{_bindir}/moto_server
%{python3_sitelib}/moto-%{version}.dist-info/
%{python3_sitelib}/moto

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Dec 17 2024 wanggang<wanggang1@kylinos.cn> - 5.0.23-1
- Update package to version 5.0.23
- New Services:
    * Kafka:
        * create_cluster()
        * create_cluster_v2()
        * describe_cluster()
        * describe_cluster_v2()
        * delete_cluster()
        * list_clusters()
        * list_clusters_v2()
        * list_tags_for_resource()
        * tag_resource()
        * untag_resource()
- New Methods:
    * DirectConnect:
        * associate_mac_sec_key()
        * create_lag()
        * describe_lags()
        * describe_settings()
        * disassociate_mac_sec_key()
        * update_settings()
    * EFS:
        * describe_file_system_policy()
        * put_file_system_policy()
    * ES:
        * describe_elasticsearch_domains()
    * OpenSearch:
        * describe_domains()
- Miscellaneous:
    * Athena: list_query_executions() now supports the WorkGroup-parameter
    * Athena: start_query_execution() now supports the WorkGroup-parameter
    * CloudFormation: AWS::IAM::Role now supports updates
    * CognitoIDP: list_users() now correctly filters before applying the Limit
    * DirectConnect: describe_trusts() no longer requires a DirectoryId-parameter
    * DynamoDB: The DeleteProtectionEnabled can now be disabled
    * DynamoDB: update_item() can now return list of binaries
    * EC2: SecurityGroups now contain a SecurityGroupArn
    * EC2: update_route() now correctly handles DestinationPrefixListId
    * KMS: get_public_key() now supports passing in aliases
    * Lambda: publish_function() now publishes a function even if the updated code hasn't changed
    * MemoryDB: tag_resource/list_tags_for_resource() now supports Snapshots and SubnetGroups
    * RDS: copy_db_snapshot() now supports the CopyTags-parameter
    * RDS: copy_db_snapshot() now accepts ARN's as the SourceSnapshotIdentifier
    * RDS: restore_db_instance_from_db_snapshot() now accepts ARN's as the SourceSnapshotIdentifier
    * S3: complete_multipart_upload() now supports IfNoneMatch-parameter

* Fri Dec 13 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 5.0.22-1
- Update package to version 5.0.22
  Add SHA1 and SHA256 algorithms for EC2 fingerprint calculation
  Adds support for UsePreviousTemplate to create_change_set
  Appsync API Cache Implementation
  Group Dependabot Updates for Java/DotNet
  Utility to iterate over flattened backends
  Add rtapi support for additional sagemaker resource
  Sagemaker Model Explainability Job Definition
  Sagemaker list endpoints and endpoint configs
  Enable pytest colored output in GitHub workflow runs
  Add RTAPI tag resource for sagemaker
  Add Full Support for ServiceDiscovery
  Sagemaker Hyperpod/Clusters Support

* Tue Jun 4 2024 lilu <lilu@kylinos.cn> - 5.0.9-1
- Update package to version 5.0.9
- Fixed an InfiniteRecursion-bug when accessing S3-buckets that was introduced in 5.0.8

* Wed May 22 2024 lilu <lilu@kylinos.cn> - 5.0.7-1
- Update package to version 5.0.7, some of the new features are as follows:
- Athena: start_query_execution() now supports the ExecutionParameters-parameter
- DynamoDB: Tables now support DeleteProtection

* Tue Apr 02 2024 wangqiang <wangqiang1@kylinos.cn> - 5.0.4-1
- Update package to version 5.0.4

* Wed Dec 21 2022 wangjunqi <wangjunqi@kylinos.cn> - 4.0.12-1
- Update package to version 4.0.12

* Thu Jun 16 2022 OpenStack_SIG <openstack@openeuler.org> - 3.0.4-1
- Upgrade version for openstack yoga

* Thu Aug 12 2021 OpenStack_SIG <openstack@openeuler.org> - 2.0.1-1
- Add BuildRequires and Requires

* Mon Aug 09 2021 OpenStack_SIG <openstack@openeuler.org> - 2.0.1-1
- Package Spec generate
